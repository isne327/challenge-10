#include <iostream>
#include "list.h"

void main()
{
	List value;
	int process, input;
	cout << "Please enter a number to proceed" << endl;
	cout << "Exit enter '0'" << endl << "Headpush enter '1'" << endl << "Tailpush enter '2'" << endl << "Headpop enter '3'" << endl << "Tailpop enter '4'" << endl << "Delete enter '5'" << endl << "Check enter '6'" << endl;
		while (true) 
		{
			cin >> process;
			if (process == 0)
			{
				break;
			}

			if (process == 1) 
			{
				cout << "Please input an integer : ";
				cin >> input;
				value.headPush(input);
				value.display();
				continue;
			}

			if (process == 2) 
			{
				cout << "Please input an integer : ";
				cin >> input;
				value.tailPush(input);
				value.display();
				continue;
			}

			if (process == 3) 
			{
				value.headPop();
				value.display();
				continue;
			}

			if (process == 4) 
			{
				value.tailPop();
				value.display();
				continue;
			}

			if (process == 5) 
			{
				value.display();
				cout << "Input a value to delete ";
				cin >> input;
				value.deleteNode(input);
				value.display();
				continue;
			}

			if (process == 6) 
			{
				cout << "Input a value to check ";
				cin >> input;
				if (value.isInList(input)) {
					cout << endl << ">>> " << input << " is in the list" << " <<<" << endl << endl;
					continue;
				}
				else {
					cout << endl << ">>> " << input << " is NOT in the list" << " <<<" << endl << endl;
					continue;
				}
			}

			else
			{
				cout << endl <<">>> Invalid input please try again <<<" << endl << endl;
				cout << "Exit enter '0'" << endl << "Headpush enter '1'" << endl << "Tailpush enter '2'" << endl << "Headpop enter '3'" << endl << "Tailpop enter '4'" << endl << "Delete enter '5'" << endl << "Check enter '6'" << endl;
				continue;
			}

		}
}