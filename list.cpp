#include <iostream>
#include "list.h"

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int el)
{
	Node *newNode;
	newNode = new Node(el);
	newNode->next = NULL;
	newNode->next = head;
	head = newNode;
}

void List::tailPush(int el)
{
	Node *newNode;
	newNode = head;
	while (newNode && newNode->next != NULL) 
	{
		newNode = newNode->next;
	}
	tail = new Node(el);
	tail->next = NULL;
	if (newNode == NULL)
		head = tail;
	else
		newNode->next = tail;
}

int List::headPop()
{
	int hold;
	Node *newNode;
	newNode = head;
	head = head->next;
	hold = newNode->info;
	delete newNode;
	return hold;
}

int List::tailPop()
{
	if (head->next == NULL) {
		delete head;
		head = NULL;
	}
	else 
	{
		int hold;
		Node *newNode;
		newNode = head;
		while (newNode->next->next != NULL) 
		{
			newNode = newNode->next;
		}
		tail = newNode->next;
		hold = newNode->info;
		delete tail;
		newNode->next = NULL;
		return hold;
	}
}

void List::deleteNode(int el)
{
	Node *newNode, *temp;
	newNode = head;
	while (newNode->next->info != el)
	{
		newNode = newNode->next;
	}
	if (newNode->next->info == el)
	{
		temp = newNode->next;
		newNode->next = temp->next;
		delete temp;
	}
}

bool List::isInList(int el)
{
	Node *newNode;
	newNode = head;
	while (newNode->info != el && newNode->next != NULL) {
		newNode = newNode->next;
	}
	if (newNode->info != el && newNode->next == NULL) {
		return false;
	}
	else
		return true;
}

void List::display()
{
	Node *nodenum;
	int count = 1;
	nodenum = head;
	cout << endl;
	cout << "****************"<<endl;
	while (nodenum != NULL) {
		cout << "Element no." <<  count << " = " << nodenum -> info << endl;
		nodenum = nodenum -> next;
		count++;
	}
	cout << "****************" << endl <<endl;
	cout << "Exit enter '0'" << endl << "Headpush enter '1'" << endl << "Tailpush enter '2'" << endl << "Headpop enter '3'" << endl << "Tailpop enter '4'" << endl << "Delete enter '5'" << endl << "Check enter '6'" << endl;
}
